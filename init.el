;;; init.el --- The init file for GNU Emacs. -*- lexical-binding: t no-byte-compile: t -*-

;; Copyright (C) 2023 Hongyu Ding

;; Author: Hongyu Ding <rainstormstudio@yahoo.com>
;; Keywords: lisp
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; GNU Emacs configuration

;;; Code:

(when (version< emacs-version "29")
  (error "Please use Emacs version 29 and above"))

;; Set deferred timer to reset them
(run-with-idle-timer
 5 nil
 (lambda ()
   (setq gc-cons-threshold gc-cons-threshold-original)
   (makunbound 'gc-cons-threshold-original)
   (message "gc-cons-threshold and file-name-handler-alist restored")))

(setq read-process-output-max (* 256 256))

;; update load path
(defun update-load-path (&rest _)
  "Update `load-path'."
  (dolist (dir '("lisp"))
    (push (expand-file-name dir user-emacs-directory) load-path)))
(update-load-path)

(defun open-config ()
  "Open the configuration files."
  (interactive)
  (find-file user-emacs-directory))

;; (require 'init-proxy)
(require 'init-packages)

;; modules
(require 'init-basics)
;; (require 'init-modal)
(require 'init-hydra)
(require 'init-org)
(require 'init-workspace)
(require 'init-dashboard)
(require 'init-ui)
(require 'init-project)
(require 'init-autocompletion)
;; (require 'init-ivy)
(require 'init-vertico)
;; (require 'init-helm)
(require 'init-git)
(require 'init-shell)
(require 'init-filetree)
(require 'init-languages)
(require 'init-lsp)
;; (require 'init-tags)
;; (require 'init-hybrid-lsp-tags)
(require 'init-tools)

;;; init.el ends here
