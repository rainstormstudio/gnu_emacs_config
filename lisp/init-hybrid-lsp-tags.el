;;; init-hybrid-lsp-tags.el --- hybrid LSP tags configurations -*- lexical-binding: t -*-

;; Copyright (C) 2023 Hongyu Ding <rainstormstudio@yahoo.com>

;; Author: Hongyu Ding <rainstormstudio@yahoo.com>
;; Keywords: lisp
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Hybrid LSP tags configurations

;;; Code:

;; (use-package flycheck
;;   :defer t
;;   :init
;;   (setq-default flycheck-emacs-lisp-load-path 'inherit)
;;   (global-flycheck-mode))

(use-package lsp-mode
  :defer t
  :init
  (setq lsp-keymap-prefix "C-c l")
  (setq lsp-headerline-breadcrumb-segments '(project file symbols))
  (setq lsp-headerline-breadcrumb-icons-enable t)
  (setq lsp-headerline-arrow (nerd-icons-octicon "nf-oct-chevron_right"))
  (setq lsp-diagnostics-provider :none)
  (setq lsp-modeline-diagnostics-enable nil)
  (setq lsp-semantic-tokens-enable t)
  (setq lsp-lens-enable nil)
  :config
  (defun my-lsp-icons-get-by-file-ext (file-ext &optional feature)
    (when (and file-ext
               (lsp-icons--enabled-for-feature feature))
      (nerd-icons-icon-for-extension file-ext)))
  (advice-add #'lsp-icons-get-by-file-ext :override #'my-lsp-icons-get-by-file-ext)
  (defvar lsp-symbol-alist
    '(
      (misc          nerd-icons-codicon "nf-cod-symbol_namespace" :face font-lock-warning-face)
      (document      nerd-icons-codicon "nf-cod-symbol_file" :face font-lock-string-face)
      (namespace     nerd-icons-codicon "nf-cod-symbol_namespace" :face font-lock-type-face)
      (string        nerd-icons-codicon "nf-cod-symbol_string" :face font-lock-doc-face)
      (boolean-data  nerd-icons-codicon "nf-cod-symbol_boolean" :face font-lock-builtin-face)
      (numeric       nerd-icons-codicon "nf-cod-symbol_numeric" :face font-lock-builtin-face)
      (method        nerd-icons-codicon "nf-cod-symbol_method" :face font-lock-function-name-face)
      (field         nerd-icons-codicon "nf-cod-symbol_field" :face font-lock-variable-name-face)
      (localvariable nerd-icons-codicon "nf-cod-symbol_variable" :face font-lock-variable-name-face)
      (class         nerd-icons-codicon "nf-cod-symbol_class" :face font-lock-type-face)
      (interface     nerd-icons-codicon "nf-cod-symbol_interface" :face font-lock-type-face)
      (property      nerd-icons-codicon "nf-cod-symbol_property" :face font-lock-variable-name-face)
      (indexer       nerd-icons-codicon "nf-cod-symbol_enum" :face font-lock-builtin-face)
      (enumerator    nerd-icons-codicon "nf-cod-symbol_enum" :face font-lock-builtin-face)
      (enumitem      nerd-icons-codicon "nf-cod-symbol_enum_member" :face font-lock-builtin-face)
      (constant      nerd-icons-codicon "nf-cod-symbol_constant" :face font-lock-constant-face)
      (structure     nerd-icons-codicon "nf-cod-symbol_structure" :face font-lock-variable-name-face)
      (event         nerd-icons-codicon "nf-cod-symbol_event" :face font-lock-warning-face)
      (operator      nerd-icons-codicon "nf-cod-symbol_operator" :face font-lock-comment-delimiter-face)
      (template      nerd-icons-codicon "nf-cod-symbol_snippet" :face font-lock-type-face)))
  (defun my-lsp-icons-get-by-symbol-kind (kind &optional feature)
    (when (and kind
               (lsp-icons--enabled-for-feature feature))
      (let* ((icon (cdr (assoc (lsp-treemacs-symbol-kind->icon kind) lsp-symbol-alist)))
             (args (cdr icon)))
        (apply (car icon) args))))
  (advice-add #'lsp-icons-get-by-symbol-kind :override #'my-lsp-icons-get-by-symbol-kind)
  :hook
  ;; (c++-mode . lsp)
  ;; (c-mode . lsp)
  (cmake-mode . lsp)
  (rust-mode . lsp)
  (lsp-mode . lsp-enable-which-key-integration)
  :commands lsp)

(use-package lsp-ui
  :defer t
  :commands lsp-ui-mode)

(use-package lsp-ivy
  :requires ivy
  :defer t
  :config
  (with-no-warnings
    (progn
      (defconst lsp-ivy-symbol-kind-icons
        `(,(nerd-icons-codicon "nf-cod-symbol_namespace") ; Unknown - 0
          ,(nerd-icons-codicon "nf-cod-symbol_file") ; File - 1
          ,(nerd-icons-codicon "nf-cod-symbol_namespace" :face 'nerd-icons-lblue) ; Module - 2
          ,(nerd-icons-codicon "nf-cod-symbol_namespace" :face 'nerd-icons-lblue) ; Namespace - 3
          ,(nerd-icons-codicon "nf-cod-package") ; Package - 4
          ,(nerd-icons-codicon "nf-cod-symbol_class" :face 'nerd-icons-orange) ; Class - 5
          ,(nerd-icons-codicon "nf-cod-symbol_method" :face 'nerd-icons-purple) ; Method - 6
          ,(nerd-icons-codicon "nf-cod-symbol_property") ; Property - 7
          ,(nerd-icons-codicon "nf-cod-symbol_field" :face 'nerd-icons-lblue) ; Field - 8
          ,(nerd-icons-codicon "nf-cod-symbol_method" :face 'nerd-icons-lpurple) ; Constructor - 9
          ,(nerd-icons-codicon "nf-cod-symbol_enum" :face 'nerd-icons-orange) ; Enum - 10
          ,(nerd-icons-codicon "nf-cod-symbol_interface" :face 'nerd-icons-lblue) ; Interface - 11
          ,(nerd-icons-codicon "nf-cod-symbol_method" :face 'nerd-icons-purple) ; Function - 12
          ,(nerd-icons-codicon "nf-cod-symbol_variable" :face 'nerd-icons-lblue) ; Variable - 13
          ,(nerd-icons-codicon "nf-cod-symbol_constant") ; Constant - 14
          ,(nerd-icons-codicon "nf-cod-symbol_string") ; String - 15
          ,(nerd-icons-codicon "nf-cod-symbol_numeric") ; Number - 16
          ,(nerd-icons-codicon "nf-cod-symbol_boolean" :face 'nerd-icons-lblue) ; Boolean - 17
          ,(nerd-icons-codicon "nf-cod-symbol_array") ; Array - 18
          ,(nerd-icons-codicon "nf-cod-symbol_class" :face 'nerd-icons-blue) ; Object - 19
          ,(nerd-icons-codicon "nf-cod-symbol_key") ; Key - 20
          ,(nerd-icons-codicon "nf-cod-symbol_numeric" :face 'nerd-icons-dsilver) ; Null - 21
          ,(nerd-icons-codicon "nf-cod-symbol_enum_member" :face 'nerd-icons-lblue) ; EnumMember - 22
          ,(nerd-icons-codicon "nf-cod-symbol_structure" :face 'nerd-icons-orange) ; Struct - 23
          ,(nerd-icons-codicon "nf-cod-symbol_event" :face 'nerd-icons-orange) ; Event - 24
          ,(nerd-icons-codicon "nf-cod-symbol_operator") ; Operator - 25
          ,(nerd-icons-codicon "nf-cod-symbol_class") ; TypeParameter - 26
          ))

      (lsp-defun my-lsp-ivy--format-symbol-match
        ((sym &as &SymbolInformation :kind :location (&Location :uri))
         project-root)
        "Convert the match returned by `lsp-mode` into a candidate string."
        (let* ((sanitized-kind (if (length> lsp-ivy-symbol-kind-icons kind) kind 0))
               (type (elt lsp-ivy-symbol-kind-icons sanitized-kind))
               (typestr (if lsp-ivy-show-symbol-kind (format "%s " type) ""))
               (pathstr (if lsp-ivy-show-symbol-filename
                            (propertize (format " · %s" (file-relative-name (lsp--uri-to-path uri) project-root))
                                        'face font-lock-comment-face)
                          "")))
          (concat typestr (lsp-render-symbol-information sym ".") pathstr)))
      (advice-add #'lsp-ivy--format-symbol-match :override #'my-lsp-ivy--format-symbol-match)))
  :commands lsp-ivy-workspace-symbol)
(use-package consult-lsp
  :requires consult)
;; (use-package lsp-treemacs
;;   :defer t
;;   :after lsp
;;   :commands lsp-treemacs-errors-list)

(use-package ccls
  :hook ((c-mode c++-mode objc-mode cuda-mode) .
         (lambda () (require 'ccls) (lsp))))

(use-package dap-mode
  :defer t
  :config
  (require 'dap-cpptools)
  (require 'dap-gdb-lldb))

(use-package ggtags
  :hook
  (prog-mode . ggtags-mode))

(elpaca-wait)

(provide 'init-hybrid-lsp-tags)
;;; init-hybrid-lsp-tags.el ends here
