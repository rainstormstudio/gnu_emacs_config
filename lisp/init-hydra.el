;;; init-hydra.el --- hydra configuration -*- lexical-binding: t -*-

;; Copyright (C) 2023 Hongyu Ding <rainstormstudio@yahoo.com>

;; Author: Hongyu Ding <rainstormstudio@yahoo.com>
;; Keywords: lisp
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; hydra configuration

;;; Code:

;; hydra
(use-package hydra
  :defer t
  :init
  ;; dynamically zoom in/out
  (defhydra hydra-zoom ()
    "zoom"
    ("=" text-scale-increase "in")
    ("-" text-scale-decrease "out")
    ("r" (text-scale-adjust 0) "reset")
    ("q" nil "quit"))

  ;; gdb debugger
  (defhydra hydra-gdb (:color pink :hint nil)
    "
^Stepping^               ^commands^
-----------------------------------------
_n_: gud-next            _b_: gud-break
_N_: gud-nexti           _c_: gud-cont
_s_: gud-step
_S_: gud-stepi           _q_: quit

"
    ("n" gud-next "step one line (skip functions)")
    ("N" gud-nexti "step one instruction (skip functions)")
    ("s" gud-step "step one source line with display")
    ("S" gud-stepi "step one instruction with display")
    ("b" gud-break "set breakpoint")
    ("c" gud-cont "continue")
    ("q" nil "quit"))
  (global-set-key (kbd "C-c d") 'hydra-gdb/body))

(elpaca-wait)

(provide 'init-hydra)
;;; init-hydra.el ends here
