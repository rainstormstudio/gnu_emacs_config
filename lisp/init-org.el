;;; init-org.el --- org related configurations -*- lexical-binding: t -*-

;; Copyright (C) 2023 Hongyu Ding <rainstormstudio@yahoo.com>

;; Author: Hongyu Ding <rainstormstudio@yahoo.com>
;; Keywords: lisp
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; org related configurations

;;; Code:

(use-package org
  :init
  (setq org-image-actual-width (list 800))
  (setq org-export-with-sub-superscripts '{}
        org-use-sub-superscripts '{})
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((plantuml . t)))
  :config
  (require 'org-indent)
  (add-to-list 'org-latex-packages-alist '("" "minted"))
  (setq org-latex-listings 'minted)
  (setq org-latex-pdf-process
        '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
          "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
          "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
  (setq org-src-fontify-natively t)
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((latex . t)))
  :bind
  (:map org-mode-map
        ("C-<return>" . #'org-meta-return)
        ("C-RET" . #'org-meta-return)
        ("M-<return>" . org-insert-heading-respect-content)
        ("M-RET" . org-insert-heading-respect-content))
  :hook
  (org-mode . visual-line-mode))

;; export to github markdown
(use-package ox-gfm
  :defer t)

;; org-modern
(use-package org-modern
  :config
  (setq
   ;; Edit settings
   org-auto-align-tags nil
   org-tags-column 0
   org-catch-invisible-edits 'show-and-error
   org-special-ctrl-a/e t
   org-insert-heading-respect-content t

   ;; Org styling, hide markup etc.
   org-hide-emphasis-markers t
   org-pretty-entities t
   org-ellipsis "…"

   ;; disable org modern table
   org-modern-table nil

   ;; Agenda styling
   org-agenda-tags-column 0
   org-agenda-block-separator ?─
   org-agenda-time-grid
   '((daily today require-timed)
     (800 1000 1200 1400 1600 1800 2000)
     " ┄┄┄┄┄ " "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄")
   org-agenda-current-time-string
   "< now ─────────────────────────────────────────────────")
  (global-org-modern-mode))

;; org-roam
(use-package org-roam
  :defer t
  :init
  (unless (file-exists-p "~/org-roam")
    (make-directory "~/org-roam"))
  (unless (file-exists-p "~/org-roam/daily")
    (make-directory "~/org-roam/daily"))
  (setq org-agenda-files '("~/org-roam" "~/org-roam/daily"))
  (setq org-modern-star (list
                         (format "%s" (nerd-icons-mdicon "nf-md-label"))
                         (format "%s" (nerd-icons-mdicon "nf-md-label_outline"))
                         ))
  :config
  (org-roam-db-autosync-mode)
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert))
  :custom
  (org-roam-directory (file-truename "~/org-roam")))

;; centering presentation
;; (use-package visual-fill-column
;;   :init
;;   (setq visual-fill-column-width 20
;;         visual-fill-column-center-text t)
;;   (defun my/org-present-start ()
;;     (visual-fill-column-mode 1)
;;     (visual-line-mode 1)
;;     (display-line-numbers-mode -1))
;;   (defun my/org-present-end ()
;;     (visual-fill-column-mode 0)
;;     (visual-line-mode 0)
;;     (display-line-numbers-mode 1)))

;; ;; org present
;; (use-package org-present
;;   :config
;;   (defun my/org-present-start ()
;;     (org-present-big)
;;     (setq org-image-actual-width (list 1400))
;;     (org-display-inline-images)
;;     (org-present-hide-cursor)
;;     (org-present-read-only))
;;   (defun my/org-present-end ()
;;     (org-present-small)
;;     (setq org-image-actual-width (list 800))
;;     (org-remove-inline-images)
;;     (org-present-show-cursor)
;;     (org-present-read-write))
;;   :hook
;;   (org-present-mode . my/org-present-start)
;;   (org-present-mode-quit . my/org-present-end)
;;   )

(use-package org-appear
  :defer t
  :hook
  (org-mode . org-appear-mode))

(use-package ox-reveal
  :defer t
  :init
  (defun org-reveal-insert-default-properties()
    "Insert the default properties for org-reveal."
    (interactive)
    (if (eq major-mode 'org-mode)
        (insert ":REVEAL_PROPERTIES:
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: serif
#+REVEAL_PLUGINS: (highlight)
#+REVEAL_INIT_OPTIONS: width:1280, height:960
#+OPTIONS: timestamp:nil toc:1 num:nil
:END:")
      (error "Not in org-mode!"))))

(elpaca-wait)

(provide 'init-org)
;;; init-org.el ends here
