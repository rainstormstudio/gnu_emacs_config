;;; init-autocompletion.el --- auto-completion configuration -*- lexical-binding: t -*-

;; Copyright (C) 2023 Hongyu Ding <rainstormstudio@yahoo.com>

;; Author: Hongyu Ding <rainstormstudio@yahoo.com>
;; Keywords: lisp
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; auto-completion configuration

;;; Code:

;; ;; filtering and sorting
;; (use-package prescient
;;   :custom
;;   (completion-styles '(prescient basic))
;;   :config
;;   (prescient-persist-mode 1))

;; ;; company
;; (use-package company
;;   :hook
;;   (elpaca-after-init . global-company-mode)
;;   :init
;;   (setq company-idle-delay 0
;;         company-echo-delay (if (display-graphic-p) nil 0)
;;         company-minimum-prefix-length 3))

;; (use-package company-box
;;   :init
;;   (setq company-box-enable-icon t
;;         company-box-doc-delay 0.1)
;;   :hook (company-mode . company-box-mode)
;;   :config
;;   (defvar company-box-icons-nerd
;;     `((Unknown       . ,(nerd-icons-codicon "nf-cod-symbol_namespace"))
;;       (Text          . ,(nerd-icons-codicon "nf-cod-symbol_string"))
;;       (Method        . ,(nerd-icons-codicon "nf-cod-symbol_method" :face 'nerd-icons-purple))
;;       (Function      . ,(nerd-icons-codicon "nf-cod-symbol_method" :face 'nerd-icons-purple))
;;       (Constructor   . ,(nerd-icons-codicon "nf-cod-symbol_method" :face 'nerd-icons-lpurple))
;;       (Field         . ,(nerd-icons-codicon "nf-cod-symbol_field" :face 'nerd-icons-lblue))
;;       (Variable      . ,(nerd-icons-codicon "nf-cod-symbol_variable" :face 'nerd-icons-lblue))
;;       (Class         . ,(nerd-icons-codicon "nf-cod-symbol_class" :face 'nerd-icons-orange))
;;       (Interface     . ,(nerd-icons-codicon "nf-cod-symbol_interface" :face 'nerd-icons-lblue))
;;       (Module        . ,(nerd-icons-codicon "nf-cod-symbol_namespace" :face 'nerd-icons-lblue))
;;       (Property      . ,(nerd-icons-codicon "nf-cod-symbol_property"))
;;       (Unit          . ,(nerd-icons-codicon "nf-cod-symbol_key"))
;;       (Value         . ,(nerd-icons-codicon "nf-cod-symbol_numeric" :face 'nerd-icons-lblue))
;;       (Enum          . ,(nerd-icons-codicon "nf-cod-symbol_enum" :face 'nerd-icons-orange))
;;       (Keyword       . ,(nerd-icons-codicon "nf-cod-symbol_keyword"))
;;       (Snippet       . ,(nerd-icons-codicon "nf-cod-symbol_snippet"))
;;       (Color         . ,(nerd-icons-codicon "nf-cod-symbol_color"))
;;       (File          . ,(nerd-icons-codicon "nf-cod-symbol_file"))
;;       (Reference     . ,(nerd-icons-codicon "nf-cod-symbol_misc"))
;;       (Folder        . ,(nerd-icons-codicon "nf-cod-folder"))
;;       (EnumMember    . ,(nerd-icons-codicon "nf-cod-symbol_enum_member" :face 'nerd-icons-lblue))
;;       (Constant      . ,(nerd-icons-codicon "nf-cod-symbol_constant"))
;;       (Struct        . ,(nerd-icons-codicon "nf-cod-symbol_structure" :face 'nerd-icons-orange))
;;       (Event         . ,(nerd-icons-codicon "nf-cod-symbol_event" :face 'nerd-icons-orange))
;;       (Operator      . ,(nerd-icons-codicon "nf-cod-symbol_operator"))
;;       (TypeParameter . ,(nerd-icons-codicon "nf-cod-symbol_class"))
;;       (Template      . ,(nerd-icons-codicon "nf-cod-symbol_snippet"))))
;;   (setq company-box-icons-alist 'company-box-icons-nerd))

;; ;; company-prescient
;; (use-package company-prescient
;;   :hook
;;   (company-mode . company-prescient-mode))

;; (require 'color)

(use-package corfu
  :config
  (setq corfu-auto t
        corfu-auto-delay 0.1
        corfu-popupinfo-delay 0
        corfu-auto-prefix 2
        global-corfu-modes '((not
                              erc-mode
                              circe-mode
                              help-mode
                              gud-mode
                              vterm-mode)
                             t)
        corfu-cycle t
        corfu-preselect 'prompt
        corfu-count 16
        corfu-max-width 120
        corfu-on-exact-match nil
        tab-always-indent 'complete)
  :hook
  (eshell-mode . (lambda()
                   (setq-local corfu-auto nil)
                   (corfu-mode)))
  :init
  (global-corfu-mode)
  (corfu-history-mode)
  (corfu-popupinfo-mode))

(use-package nerd-icons-corfu
  :after corfu
  :config
  (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))

;; (use-package cape
;;   :bind ("M-p" . cape-prefix-map)
;;   :init
;;   (add-hook 'completion-at-point-functions #'cape-dabbrev)
;;   (add-hook 'completion-at-point-functions #'cape-file))

;; (use-package kind-icon
;;   :after corfu
;;   :custom
;;   (kind-icon-default-face 'corfu-default)
;;   :init
;;   (setq kind-icon-use-icons nil)

;;   (defsubst rgb-blend (rgb1 rgb2 frac)
;;     (apply #'color-rgb-to-hex
;;            (cl-mapcar (lambda (a b)
;;                         (+ (* a frac) (* b (- 1.0 frac))))
;;                       rgb1 rgb2)))

;;   (defsubst icon-face-spec (face)
;;     (let* ((default-bg (frame-parameter nil 'background-color))
;;            (fg (or (face-foreground face) (frame-parameter nil 'foreground-color)))
;;            (bg (rgb-blend (color-name-to-rgb fg) (color-name-to-rgb default-bg) 0.12)))
;;       `(:foreground ,fg :background ,bg)))

;;   (defun codicon (name face)
;;     (nerd-icons-codicon name :face (icon-face-spec face)))

;;   (defconst corfu-kind-icon-mapping
;;     `(
;;       (array .          ,(codicon "nf-cod-symbol_array"       'font-lock-type-face))
;;       (boolean .        ,(codicon "nf-cod-symbol_boolean"     'font-lock-builtin-face))
;;       (class .          ,(codicon "nf-cod-symbol_class"       'font-lock-type-face))
;;       (color .          ,(codicon "nf-cod-symbol_color"       'success) )
;;       (command .        ,(codicon "nf-cod-terminal"           'default) )
;;       (constant .       ,(codicon "nf-cod-symbol_constant"    'font-lock-constant-face) )
;;       (constructor .    ,(codicon "nf-cod-triangle_right"     'font-lock-function-name-face) )
;;       (enummember .     ,(codicon "nf-cod-symbol_enum_member" 'font-lock-builtin-face) )
;;       (enum-member .    ,(codicon "nf-cod-symbol_enum_member" 'font-lock-builtin-face) )
;;       (enum .           ,(codicon "nf-cod-symbol_enum"        'font-lock-builtin-face) )
;;       (event .          ,(codicon "nf-cod-symbol_event"       'font-lock-warning-face) )
;;       (field .          ,(codicon "nf-cod-symbol_field"       'font-lock-variable-name-face) )
;;       (file .           ,(codicon "nf-cod-symbol_file"        'font-lock-string-face) )
;;       (folder .         ,(codicon "nf-cod-folder"             'font-lock-doc-face) )
;;       (interface .      ,(codicon "nf-cod-symbol_interface"   'font-lock-type-face) )
;;       (keyword .        ,(codicon "nf-cod-symbol_keyword"     'font-lock-keyword-face) )
;;       (macro .          ,(codicon "nf-cod-symbol_misc"        'font-lock-keyword-face) )
;;       (magic .          ,(codicon "nf-cod-wand"               'font-lock-builtin-face) )
;;       (method .         ,(codicon "nf-cod-symbol_method"      'font-lock-function-name-face) )
;;       (function .       ,(codicon "nf-cod-symbol_method"      'font-lock-function-name-face) )
;;       (module .         ,(codicon "nf-cod-file_submodule"     'font-lock-preprocessor-face) )
;;       (numeric .        ,(codicon "nf-cod-symbol_numeric"     'font-lock-builtin-face) )
;;       (operator .       ,(codicon "nf-cod-symbol_operator"    'font-lock-comment-delimiter-face) )
;;       (param .          ,(codicon "nf-cod-symbol_parameter"   'default) )
;;       (property .       ,(codicon "nf-cod-symbol_property"    'font-lock-variable-name-face) )
;;       (reference .      ,(codicon "nf-cod-references"         'font-lock-variable-name-face) )
;;       (snippet .        ,(codicon "nf-cod-symbol_snippet"     'font-lock-string-face) )
;;       (string .         ,(codicon "nf-cod-symbol_string"      'font-lock-string-face) )
;;       (struct .         ,(codicon "nf-cod-symbol_structure"   'font-lock-variable-name-face) )
;;       (text .           ,(codicon "nf-cod-text_size"          'font-lock-doc-face) )
;;       (typeparameter .  ,(codicon "nf-cod-list_unordered"     'font-lock-type-face) )
;;       (type-parameter . ,(codicon "nf-cod-list_unordered"     'font-lock-type-face) )
;;       (unit .           ,(codicon "nf-cod-symbol_ruler"       'font-lock-constant-face) )
;;       (value .          ,(codicon "nf-cod-symbol_field"       'font-lock-builtin-face) )
;;       (variable .       ,(codicon "nf-cod-symbol_variable"    'font-lock-variable-name-face) )
;;       (t .              ,(codicon "nf-cod-code"               'font-lock-warning-face))))

;;   (defsubst nerd-icon--metadata-get (metadata type-name)
;;     (or
;;      (plist-get completion-extra-properties (intern (format ":%s" type-name)))
;;      (cdr (assq (intern type-name) metadata))))

;;   (defsubst nerd-icon-formatted (kind)
;;     (let* ((icon (alist-get kind corfu-kind-icon-mapping))
;;            (icon-face (get-text-property 0 'face icon))
;;            (icon-bg (plist-get icon-face :inherit))
;;            (icon-pad (propertize " " 'face (append '(:height 0.5) icon-bg)))
;;            (item-pad (propertize " " 'face '(:height 0.5))))
;;       (concat icon-pad icon icon-pad item-pad)))

;;   (defun nerd-icon-margin-formatter (metadata)
;;     (if-let ((kind-func (nerd-icon--metadata-get metadata "company-kind")))
;;         (lambda (cand)
;;           (if-let ((kind (funcall kind-func cand)))
;;               (nerd-icon-formatted kind)
;;             (nerd-icon-formatted t)))))
;;   :config
;;   (add-to-list 'corfu-margin-formatters #'nerd-icon-margin-formatter))

;; (unless (display-graphic-p)
;;   (progn
;;     (use-package corfu-terminal
;;       :config
;;       (corfu-terminal-mode +1))))

(elpaca-wait)

(provide 'init-autocompletion)
;;; init-autocompletion.el ends here
