;;; init-ivy.el --- ivy configuration -*- lexical-binding: t -*-

;; Copyright (C) 2023 Hongyu Ding <rainstormstudio@yahoo.com>

;; Author: Hongyu Ding <rainstormstudio@yahoo.com>
;; Keywords: lisp
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; ivy configuration

;;; Code:

;; counsel
(use-package counsel
  :diminish counsel-mode
  :hook
  ((ivy-mode . counsel-mode))
  :bind (("M-x" . counsel-M-x)
         ("C-x b" . counsel-switch-buffer)
         ("C-x C-f" . counsel-find-file)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history)))

;; ivy
(use-package ivy
  :diminish ivy-mode
  :init
  (setq ivy-initial-inputs-alist nil)
  :hook
  ((elpaca-after-init . ivy-mode)))

;; ivy-posframe
(use-package ivy-posframe
  :after ivy
  :diminish ivy-posframe-mode
  :init
  (defun my/ivy-posframe-get-size ()
    "Set the ivy-posframe size according to the current frame."
    (let ((height (or ivy-posframe-height (or ivy-height 10)))
          (width (min (or ivy-posframe-width 200) (round (* 0.8 (frame-width))))))
      (list :height height :width width :min-height height :min-width width)))
  (setq ivy-posframe-size-function 'my/ivy-posframe-get-size)
  (setq ivy-posframe-display-functions-alist
        '((t . ivy-posframe-display-at-frame-top-center)))
  :config
  (ivy-posframe-mode 1))

;; swiper
(use-package swiper
  :defer t
  :bind (("C-s" . swiper)))

;; ivy-rich
(use-package ivy-rich
  :hook
  ((ivy-mode . ivy-rich-mode)))

;; all-the-icons-ivy
(use-package nerd-icons-ivy-rich
  :hook
  ((ivy-mode . nerd-icons-ivy-rich-mode)))

;; ivy-prescient
(use-package ivy-prescient
  :init
  (setq ivy-prescient-enable-filtering nil
        ivy-prescient-retain-classic-highlighting t)
  :hook
  (ivy-mode . ivy-prescient-mode))

;; (use-package counsel-projectile
;;   :hook
;;   ((counsel-mode . counsel-projectile-mode)))

;; helpful
(use-package helpful
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

(elpaca-wait)

(provide 'init-ivy)
;;; init-ivy.el ends here
