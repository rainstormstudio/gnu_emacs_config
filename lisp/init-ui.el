;;; init-ui.el --- UI configurations -*- lexical-binding: t -*-

;; Copyright (C) 2023 Hongyu Ding <rainstormstudio@yahoo.com>

;; Author: Hongyu Ding <rainstormstudio@yahoo.com>
;; Keywords: lisp
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; UI configurations

;;; Code:

;; beacon
;; (use-package beacon
;;   :hook
;;   (prog-mode . beacon-mode))

;; pulsar
(if (display-graphic-p)
    (use-package pulsar
      :init
      (setq pulsar-pulse t)
      (setq pulsar-delay 0.055)
      (setq pulsar-iterations 10)
      (defface my/pulsar-background
        '((default :extend t)
          (((class color) (background light)) (:background "black"))
          (((class color) (background dark)) (:background "white")))
        "Background color face for pulsar.")
      (setq pulsar-face 'my/pulsar-background)
      (setq pulsar-highlight-face 'pulsar-yellow)
      :config
      (pulsar-global-mode)))

;; theme
;; (use-package color-theme-sanityinc-tomorrow
;;   :config (color-theme-sanityinc-tomorrow-night))
;; (use-package exotica-theme
;;   :config (load-theme 'exotica t))
;; (use-package atom-one-dark-theme
;;   :config (load-theme 'atom-one-dark t))
;; (use-package gruvbox-theme
;;   :config
;;   (load-theme 'gruvbox-dark-hard))
;; (use-package modus-themes
;;   :config
;;   ;; (setq modus-themes-common-palette-overrides
;;   ;;       modus-themes-preset-overrides-intense)
;;   (setq modus-themes-italic-constructs t
;;         modus-themes-bold-constructs t
;;         modus-themes-mixed-fonts t
;;         modus-themes-variable-pitch-ui t)
;;   (setq modus-themes-common-palette-overrides
;;       `(
;;         (border-mode-line-active blue-intense)
;;         (border-mode-line-inactive blue-intense)
;;         ,@modus-themes-preset-overrides-faint))
;;   (load-theme 'modus-vivendi-tinted :no-confirm))
;; (use-package kaolin-themes
;;   :config
;;   (load-theme 'kaolin-aurora t))
;; (use-package doom-themes
;;   :config
;;   (setq doom-themes-enable-bold t
;;         doom-themes-enable-italic t)
;;   (load-theme 'doom-city-lights t)
;;   (doom-themes-org-config))
(use-package catppuccin-theme
  :custom
  (catppuccin-enlarge-headings nil)
  :config
  (setq catppuccin-flavor 'mocha)
  (load-theme 'catppuccin t))

(use-package solaire-mode
  :hook
  (elpaca-after-init . solaire-global-mode))

(use-package emacs
  :ensure nil
  :config
  (defun my/modeline-render (left right)
    (let ((available-width (- (window-total-width)
                              (+ (length (format-mode-line left))
                                 (length (format-mode-line right))))))
      (append left
              (list (format (format "%%%ds" available-width) ""))
              right)))
  (defun my/modeline--buffer-status ()
    (cond (buffer-read-only
           (propertize " " 'face (list :background (face-foreground 'warning))))
          ((buffer-modified-p)
           (propertize " " 'face (list :background (face-foreground 'error))))
          (t
           (propertize " " 'face (list :background (face-foreground 'success))))))
  (defun my/modeline--vc ()
    (when (and vc-mode buffer-file-name)
      (let* ((backend (vc-backend buffer-file-name))
             (state (vc-state buffer-file-name backend))
             (str (if vc-display-status
                      (substring vc-mode (+ (if (eq backend 'Hg) 2 3) 2))
                    ""))
             (face (cond ((eq state 'needs-update)
                          'font-lock-warning-face)
                         ((memq state '(removed conflict unregistered))
                          'error)
                         (t 'mode-line))))
        (concat "@" str))))
  (defun my/modeline--major-mode ()
    mode-name)
  (defun my/modeline--process ()
    mode-line-process)
  (setq-default mode-line-format
                '((:eval
                   (my/modeline-render
                    ;; left
                    (quote ((:eval (my/modeline--buffer-status))
                            " "
                            (:eval (nerd-icons-icon-for-buffer))
                            " "
                            "%b"
                            " "
                            (:eval (my/modeline--vc))
                            ))
                    ;; right
                    (quote ((:eval (my/modeline--major-mode))
                            (:eval (my/modeline--process))
                            " "
                            "%p"
                            " ")))))))
;; mode-line
;; (use-package spaceline
;;   :straight (spaceline :type git :host github
;;                        :repo "rainstormstudio/spaceline")
;;   :after winum
;;   :init
;;   (setq
;;    powerline-default-separator 'utf-8
;;    ;;spaceline-highlight-face-func 'spaceline-highlight-face-modified
;;    spaceline-window-numbers-unicode t
;;    )
;;   :config
;;   (spaceline-emacs-theme))
;; (use-package doom-modeline
;;   :init
;;   (setq doom-modeline-height 14
;;         doom-modeline-hud t
;;         doom-modeline-project-detection 'project)
;;   :hook
;;   (elpaca-after-init . doom-modeline-mode))

;; ibuffer
(use-package nerd-icons-ibuffer
  :hook
  (ibuffer-mode . nerd-icons-ibuffer-mode))

;; ligatures
(if (and (display-graphic-p) (eq system-type 'gnu/linux))
    (use-package fira-code-mode
      :diminish fira-code-mode
      :custom
      (fira-code-mode-disabled-ligatures '("[]" "x"))
      :config
      (fira-code-mode-set-font)
      :hook prog-mode))

;; rainbow delimiters
(use-package rainbow-delimiters
  :defer t
  :hook (prog-mode . rainbow-delimiters-mode))

;; highlight todos
(use-package hl-todo
  :config
  (global-hl-todo-mode 1))

;; indent guide
;; (use-package highlight-indent-guides
;;   :custom
;;   (highlight-indent-guides-method 'character)
;;   (highlight-indent-guides-responsive 'top)
;;   (highlight-indent-guides-auto-character-face-perc 30)
;;   (highlight-indent-guides-auto-top-character-face-perc 80)
;;   :config
;;   :hook
;;   (prog-mode . highlight-indent-guides-mode))
(use-package indent-bars
  :ensure (indent-bars :type git :host github :repo "jdtsmith/indent-bars")
  :custom
  (indent-bars-treesit-support t)
  (indent-bars-treesit-ignore-blank-lines-types '("module"))
  (indent-bars-color-by-depth nil)
  (indent-bars-color '(highlight :face-bg t :blend 0.2))
  (indent-bars-highlight-current-depth (list :face 'highlight :face-bg t :blend 0.5))
  (indent-bars-prefer-character t)
  (indent-bars-highlight-selection-method nil)
  :hook (prog-mode . indent-bars-mode))

(use-package unicode-fonts
  :config
  (unicode-fonts-setup))

(elpaca-wait)

(provide 'init-ui)
;;; init-ui.el ends here
