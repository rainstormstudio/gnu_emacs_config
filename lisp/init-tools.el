;;; init-tools.el --- basic configurations -*- lexical-binding: t -*-

;; Copyright (C) 2023 Hongyu Ding <rainstormstudio@yahoo.com>

;; Author: Hongyu Ding <rainstormstudio@yahoo.com>
;; Keywords: lisp
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; some helpful tools

;;; Code:

;; rainbow mode
(use-package rainbow-mode
  :defer t)

;; profile startup time
(use-package esup
  :defer t)

;; package-lint
(use-package package-lint
  :defer t)

;; pdf-tools
(use-package pdf-tools
  :config
  (pdf-loader-install))

;; ztree
(use-package ztree
  :defer t)

(elpaca-wait)

(provide 'init-tools)
;; init-tools.el ends here
