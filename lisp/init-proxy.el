(setq url-proxy-services
      '(("no_proxy" . "^\\(localhost\\|10\\..*\\|192\\.168\\..*\\)")
        ("http" . "proxy.com:8080")
        ("https" . "proxy.com:8080")))

(setq url-http-proxy-basic-auth-storage
      (list (list "proxy.com:8080"
                  (cons "account name"
                        (base64-encode-string "password")))))


(provide 'init-proxy)
