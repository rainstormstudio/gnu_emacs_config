;;; init-project.el --- project configuration -*- lexical-binding: t -*-

;; Copyright (C) 2023 Hongyu Ding <rainstormstudio@yahoo.com>

;; Author: Hongyu Ding <rainstormstudio@yahoo.com>
;; Keywords: lisp
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; project configuration

;;; Code:

;; project management
(use-package project
  :ensure nil
  :bind-keymap
  ("C-c p" . project-prefix-map))
;; (use-package projectile
;;   :config (projectile-mode)
;;   :bind-keymap
;;   ("C-c p" . projectile-command-map)
;;   :init
;;   (setq projectile-project-root-files-bottom-up
;;         (append '(".projectile"
;;                   ".project"
;;                   ".git")
;;                 (when (executable-find "hg")
;;                   '(".hg"))
;;                 (when (executable-find "bzr")
;;                   '(".bzr")))
;;         projectile-project-root-files '()
;;         projectile-project-root-files-top-down-recurring '("Makefile"))
;;   (setq projectile-enable-caching t
;;         projectile-auto-discover nil
;;         projectile-globally-ignored-files '(".DS_Store" "TAGS")
;;         projectile-globally-ignored-file-suffixes '(".elc" ".pyc" ".o")
;;         projectile-kill-buffers-filter 'kill-only-files)
;;   (setq projectile-completion-system 'default)
;;   (setq projectile-switch-project-action #'projectile-dired))

(elpaca-wait)

(provide 'init-project)
;;; init-project.el ends here
