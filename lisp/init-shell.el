;;; init-shell.el --- shell configuration -*- lexical-binding: t -*-

;; Copyright (C) 2023 Hongyu Ding <rainstormstudio@yahoo.com>

;; Author: Hongyu Ding <rainstormstudio@yahoo.com>
;; Keywords: lisp
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; shell configuration

;;; Code:

;; (if (eq system-type 'gnu/linux)
;;     (progn
;;       (use-package vterm
;;         :defer t)
;;       (use-package vterm-toggle
;;         :defer t
;;         :config
;;         (setq vterm-toggle-fullscreen-p nil)
;;         (add-to-list 'display-buffer-alist
;;                      '((lambda (buffer-or-name _)
;;                          (let ((buffer (get-buffer buffer-or-name)))
;;                            (with-current-buffer buffer
;;                              (or (equal major-mode 'vterm-mode)
;;                                  (string-prefix-p vterm-buffer-name (buffer-name buffer))))))
;;                        (display-buffer-reuse-window display-buffer-at-bottom)
;;                        (reusable-frames . visible)
;;                        (window-height . 0.3)))
;;         :bind
;;         (("C-c t" . vterm-toggle-cd))))
(use-package eshell-toggle
  :defer t
  :custom
  (eshell-toggle-size-fraction 3)
  (eshell-toggle-use-projectile-root nil)
  (eshell-toggle-run-command nil)
  :bind
  (("C-c t" . eshell-toggle)))

(use-package emacs
  :ensure nil
  :config
  (setq eshell-banner-message
        '(format "%s %s\n\n"
                 (propertize (format " %s " (string-trim (buffer-name)))
                             'face 'mode-line-highlight)
                 (propertize (current-time-string)
                             'face 'font-lock-keyword-face)))
  (setq eshell-scroll-to-bottom-on-input 'all)
  (setq eshell-scroll-to-bottom-on-output 'all)
  (setq eshell-kill-processes-on-exit t)
  (setq eshell-hist-ignoredups t)
  (setq eshell-error-if-no-glob t)
  (add-hook 'eshell-mode-hook (lambda ()
                                (setq-local global-hl-line-mode nil)
                                (set-window-fringes nil 0 0)
                                (set-window-margins nil 1 nil)))
  (setq eshell-cmpl-cycle-completions nil)
  (setq eshell-cmpl-ignore-case t)
  (setq eshell-prompt-regexp "^[^#$\n]*[#$] "
        eshell-prompt-function
        (lambda nil
          (concat
           (propertize (eshell/pwd) 'face `(:background "yellow" :foreground "black"))
           (propertize "\n" 'face 'term)
           (propertize (format "[%s]" user-login-name) 'face `(:foreground "green"))
           (propertize "@" 'face 'term)
           (propertize (format "[%s]" system-name) 'face `(:foreground "cyan"))
           (propertize " - " 'face 'term)
           (propertize (format-time-string "[%a %T]") 'face `(:foreground "magenta"))
           (propertize "\n" 'face 'term)
	   (if (= (user-uid) 0)
               (propertize "#" 'face `(:foreground "red"))
             (propertize "$" 'face `(:foreground "orange")))
           (propertize " " 'face 'term))))

  ;; disable company in eshell
  ;; (add-hook 'eshell-mode-hook (lambda() (progn
  ;;                                         (company-mode 0)
  ;;                                         (ivy-mode 0))))
  )

(elpaca-wait)

(provide 'init-shell)
;;; init-shell.el ends here
