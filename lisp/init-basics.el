;;; init-basics.el --- basic configurations -*- lexical-binding: t -*-

;; Copyright (C) 2023 Hongyu Ding <rainstormstudio@yahoo.com>

;; Author: Hongyu Ding <rainstormstudio@yahoo.com>
;; Keywords: lisp
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; basic configurations

;;; Code:

(use-package emacs
  :ensure nil
  :config
  ;; disable bell alert
  (setq uniquify-buffer-name-style 'forward
	ring-bell-function #'ignore
	visible-bell nil)

  (setq inhibit-startup-message t
        initial-scratch-message ""
        save-abbrevs 'silently)
  (defun display-startup-echo-area-message ()
    (message (pwd)))

  ;; backups
  (setq create-lockfiles nil
	make-backup-files nil
	version-control t
	backup-by-copying t
	delete-old-versions t
	kept-old-versions 5
	kept-new-versions 5)
  (unless backup-directory-alist
    (setq backup-directory-alist `(("." . ,(concat user-emacs-directory
						   "backups")))))
  (setq tramp-backup-directory-alist backup-directory-alist)

  (savehist-mode 1)

  ;; auto save
  (setq auto-save-default t
	auto-save-include-big-deletions t
	auto-save-list-file-prefix (concat user-emacs-directory "autosave/")
	tramp-auto-save-directory (concat user-emacs-directory "tramp-autosave/")
	auto-save-file-name-transforms
	(list (list "\\`/[^/]*:\\([^/]*/\\)*\\([^/]*\\)\\'"
                    ;; Prefix tramp autosaves to prevent conflicts with local ones
                    (concat auto-save-list-file-prefix "tramp-\\2") t)
              (list ".*" auto-save-list-file-prefix t)))

  ;; confirmation
  (setq confirm-nonexistent-file-or-buffer nil)

  (add-hook 'text-mode-hook #'visual-line-mode)

  (setq kill-do-not-save-duplicates t)

  ;; frame
  (setq frame-title-format '("%b - Emeowacs")
	icon-title-format frame-title-format)
  (setq frame-resize-pixelwise t)
  (setq window-resize-pixelwise nil)

  (setq use-dialog-box nil)
  (when (bound-and-true-p tooltip-mode)
    (tooltip-mode -1))

  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (horizontal-scroll-bar-mode -1)

  (setq uniquify-buffer-name-style 'forward)

  (save-place-mode 1)

  (setq window-divider-default-places t
	window-divider-default-bottom-width 1
	window-divider-default-right-width 1)
  (window-divider-mode 1)

  (setq split-width-threshold 160
	split-height-threshold nil)

  (setq resize-mini-windows 'grow-only)

  ;; yes/no
  (if (boundp 'use-short-answers)
      (setq use-short-answers t)
    (advice-add #'yes-or-no-p :override #'y-or-n-p))

  ;; keep the cursor out of the read-only portions of the minibuffer
  (setq minibuffer-prompt-properties '(read-only t intangible t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; comint
  (setq comint-prompt-read-only t
	comint-buffer-maximum-size 2048)

  ;; keybindings
  (global-set-key (kbd "M-/") 'hippie-expand)
  (global-set-key (kbd "C-x C-b") 'ibuffer)
  (global-set-key (kbd "M-z") 'zap-up-to-char)

  (global-set-key (kbd "C-s") 'isearch-forward-regexp)
  (global-set-key (kbd "C-r") 'isearch-backward-regexp)
  (global-set-key (kbd "C-M-s") 'isearch-forward)
  (global-set-key (kbd "C-M-r") 'isearch-backward)

  ;; resolve symlinks
  (setq find-file-visit-truename t
	vc-follow-symlinks t)

  (setq find-file-suppress-same-file-warnings t)

  ;; highlight line
  (defvar global-hl-line-modes
    '(prog-mode text-mode conf-mode special-mode
		org-agenda-mode dired-mode))
  (global-hl-line-mode 1)

  ;; truncate lines
  (setq-default word-wrap t)
  (setq-default truncate-lines t)
  (setq truncate-partial-width-windows nil)
  (setq sentence-end-double-space nil)

  ;; compile
  (setq compilation-always-kill t
	compilation-ask-about-save nil
	compilation-scroll-output t)
  (autoload 'comint-truncate-buffer "comint" nil t)
  (add-hook 'compilation-filter-hook #'comint-truncate-buffer)

  (add-hook 'compilation-filter-hook 'ansi-color-compilation-filter)

  ;; line numbers
  (setq-default display-line-numbers-width 3)
  (setq-default display-line-numbers-widen t)
  (setq display-line-numbers-type 'relative)
  (add-hook 'prog-mode-hook #'display-line-numbers-mode)
  (add-hook 'text-mode-hook #'display-line-numbers-mode)
  (add-hook 'conf-mode-hook #'display-line-numbers-mode)

  ;; coding system
  (prefer-coding-system 'utf-8)

  ;; window size
  (when window-system (set-frame-size (selected-frame) 100 40))

  ;; fonts
  (set-face-attribute 'default nil :font "CaskaydiaCove NF-11")
  (set-fontset-font t nil (font-spec :name "Dejavu Sans"))

  ;; tty support
  (xterm-mouse-mode 1)
  (setq xterm-set-window-title t)
  (setq visible-cursor nil)

  ;; disable ido mode
  (ido-mode -1)

  ;; xref
  (setq xref-prompt-for-identifier nil)

  ;; recent files
  (recentf-mode 1)
  (setq recentf-max-menu-items 25)
  (setq recentf-max-saved-items 25)
  (setq recentf-auto-cleanup 'never)

  ;; mouse improvements
  (blink-cursor-mode -1)
  (setq mouse-wheel-scroll-amount '(1 ((shift) . 1) ((control) . nil)))
  (setq mouse-wheel-progressive-speed nil)
  (setq mouse-wheel-follow-mouse 't)
  (setq scroll-step 1)

  ;; scroll improvements
  (setq hscroll-margin 2)
  (setq hscroll-step 1)
  (setq scroll-conservatively 10)
  (setq scroll-preserve-screen-position t)
  (setq auto-window-vscroll nil)
  (setq scroll-margin 4)
  (setq scroll-conservatively most-positive-fixnum)
  (setq pixel-scroll-precision-large-scroll-height 40.0)
  (pixel-scroll-precision-mode)

  (setq blink-matching-paren nil)
  (setq x-stretch-cursor nil)

  ;; auto pair brackets
  (electric-pair-mode 1)
  (defvar electric-pair-preserve-balance nil)

  ;; auto-revert
  (global-auto-revert-mode)

  ;; fringe mode
  (setq-default left-fringe-width 3)
  (setq-default right-fringe-width 8)
  (fringe-mode 1)

  ;; show trailing whitespace
  (add-hook 'prog-mode-hook (lambda () (setq show-trailing-whitespace t)))

  ;; debugging
  (setq gdb-many-windows t)

  ;; build
  (defun build-project ()
    "Run compile interactively."
    (interactive)
    (let ((current-prefix-arg '(4)))
      (call-interactively 'compile)
      (switch-to-buffer-other-window "*compilation*")
      (end-of-buffer)))

  ;; window layouts
  (setq display-buffer-alist
        '(("\\*\\(Backtrace\\|Warnings\\|Compile-Log\\|[Hh]elp\\|Messages\\)\\*"
           (display-buffer-in-side-window)
           (window-height . 0.4)
           (side . bottom)
           (slot . 0))
          ))

  ;; Set transparency of emacs
  (defun transparency (value)
    "Sets the transparency of the frame window. 0=transparent/100=opaque"
    (interactive "nTransparency Value 0 - 100 opaque:")
    (set-frame-parameter nil 'alpha-background value)
    (add-to-list 'default-frame-alist '(alpha-background . value)))

  (setq-default indent-tabs-mode nil)
  (setq save-interprogram-paste-before-kill t
	apropos-do-all t
	mouse-yank-at-point t
	require-final-newline t
	load-prefer-newer t
	frame-inhibit-implied-resize t)

  ;; ediff
  (setq ediff-diff-options "-w"
	ediff-split-window-function #'split-window-horizontally
	ediff-window-setup-function #'ediff-setup-windows-plain)

  ;; extra file extensions
  (nconc
   auto-mode-alist
   '(("/LICENSE\\'" . text-mode)
     ("\\.log\\'" . text-mode)
     ("rc\\'" . conf-mode)
     ("\\.\\(?:hex\\|nes\\)\\'" . hexl-mode)))

  ;; custom file
  (setq custom-file (expand-file-name "custom.el" user-emacs-directory)))

;; icons
(use-package all-the-icons
  :init
  (setq all-the-icons-scale-factor 1.0))

(use-package nerd-icons)

;; multiple cursors
(use-package multiple-cursors
  :defer t)

;; anzu
(use-package anzu
  :config
  (global-anzu-mode +1))

;; window number
(use-package ace-window
  :init
  (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
  :bind
  (("C-x o" . ace-window)))

;; transient
(use-package transient
  :defer t)

;; undo
(use-package vundo
  :bind (("C-x u" . vundo)))

;; xclip
(use-package xclip
  :config (xclip-mode))

;; which-key
(use-package which-key
  :defer t
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.5))

;; search
(use-package rg
  :init
  (rg-enable-default-bindings))

;; wgrep
(use-package wgrep
  :config
  (setq wgrep-auto-save-buffer t))

;; expand-region
(use-package expand-region
  :bind
  (("C-=" . er/expand-region)))

(use-package hide-mode-line
  :defer t)

;; open large files
(use-package vlf
  :init (require 'vlf-setup))

(elpaca-wait)

(provide 'init-basics)
;;; init-basics.el ends here

