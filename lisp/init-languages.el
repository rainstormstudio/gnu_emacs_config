;;; init-languages.el --- languages configurations -*- lexical-binding: t -*-

;; Copyright (C) 2023 Hongyu Ding <rainstormstudio@yahoo.com>

;; Author: Hongyu Ding <rainstormstudio@yahoo.com>
;; Keywords: lisp
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; languages configurations

;;; Code:

(use-package yasnippet
  :config
  (yas-global-mode 1))

(use-package yasnippet-snippets
  :defer t)

;; cc-mode
(setq-default c-default-style "stroustrup")

;; yaml
(use-package yaml-mode
  :defer t)

;; rust
(use-package rust-mode
  :defer t)

;; cmake
(use-package cmake-mode
  :defer t)

;; meson build
(use-package meson-mode
  :defer t
  :after company
  :config
  (add-hook 'meson-mode-hook 'company-mode))

;; markdown
(use-package markdown-mode
  :defer t)

;; csv
(use-package csv-mode
  :defer t
  :hook
  (csv-mode . csv-align-mode))

;; lua
(use-package lua-mode
  :defer t)

;; plantuml
(use-package plantuml-mode
  :defer t
  :init
  (setq
   plantuml-jar-path (expand-file-name "~/plantuml.jar")
   plantuml-default-exec-mode 'jar
   plantuml-output-type "png"
   org-plantuml-jar-path plantuml-jar-path))

;; syntax highlighting
;; (use-package tree-sitter
;;   :diminish tree-sitter-mode
;;   :config
;;   (global-tree-sitter-mode)
;;   ;;(add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode)
;;   (setq tree-sitter-debug-jump-buttons t
;;         tree-sitter-debug-highlight-jump-region t))
;; (use-package tree-sitter-langs
;;   :after tree-sitter)
(use-package treesit-auto
  :init
  (setq treesit-font-lock-level 4)
  :custom
  (treesit-auto-install 'prompt)
  :config
  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode))

(elpaca-wait)

(provide 'init-languages)
;;; init-languages.el ends here
