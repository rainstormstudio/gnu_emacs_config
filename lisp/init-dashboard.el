;;; init-dashboard.el --- dashboard configuration -*- lexical-binding: t -*-

;; Copyright (C) 2023 Hongyu Ding <rainstormstudio@yahoo.com>

;; Author: Hongyu Ding <rainstormstudio@yahoo.com>
;; Keywords: lisp
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; dashboard configuration

;;; Code:

;; dashboard
(use-package dashboard
  :init
  (setq ;; initial-buffer-choice (lambda () (get-buffer-create "*dashboard*"))
   dashboard-center-content t
   dashboard-display-icons-p t
   dashboard-set-navigator t
   dashboard-set-init-info t
   dashboard-projects-backend 'project-el
   dashboard-navigator-buttons `(
                                 ((,(format "%s" (nerd-icons-faicon "nf-fa-gitlab")) " Gitlab" "Open gitlab link"
                                   (lambda (&rest _) (browse-url "https://gitlab.com/rainstormstudio")))
                                  (,(format "%s" (nerd-icons-codicon "nf-cod-github")) " Github" "Open github link"
                                   (lambda (&rest _) (browse-url "https://github.com/rainstormstudio")))
                                  (,(format "%s" (nerd-icons-codicon "nf-cod-settings_gear")) " Settings" "Open init file"
                                   (lambda (&rest _) (find-file user-emacs-directory)))
                                  (,(format "%s" (nerd-icons-faicon "nf-fa-refresh")) " Update Packages" "Update packages"
                                   (lambda (&rest _) (elpaca-status)))
                                  ))
   dashboard-set-footer t
   dashboard-banner-logo-title "[ E M E O W A C S ]"
   dashboard-footer-messages '("Fission Mailed!")
   dashboard-footer-icon (format "%s " (nerd-icons-octicon "nf-oct-terminal" :face '(:foreground "#03f4fc")))
   dashboard-set-heading-icons t
   dashboard-icon-type 'nerd-icons
   dashboard-set-file-icons t
   dashboard-page-separator "\n\f\n"
   dashboard-startup-banner (concat user-emacs-directory "banners/banner-5.txt")
   dashboard-projects-switch-function #'project-switch-project
   dashboard-items '((recents . 5)
                     (agenda . 5)
                     (projects . 5)))
  :config
  (dashboard-setup-startup-hook))

(use-package page-break-lines
  :diminish page-break-lines-mode
  :config
  (global-page-break-lines-mode))

(elpaca-wait)

(provide 'init-dashboard)
;;; init-dashboard.el ends here
